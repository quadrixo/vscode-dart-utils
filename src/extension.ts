import * as vscode from 'vscode';
import { ProjectQuickPickItem } from './internal';

let terminal: vscode.Terminal | null = null;

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
	let disposable = vscode.commands.registerCommand('webdev.serve', webdevServe);

	context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {}

async function webdevServe() {
	const files = await vscode.workspace.findFiles("pubspec.yaml", "**/.dart_tool/**");

	const pickItems = files.map(file => {
		const folder = vscode.workspace.getWorkspaceFolder(file) as vscode.WorkspaceFolder;
		return <ProjectQuickPickItem>{
			label: folder.name,
			description: folder.uri.fsPath,
			folder: folder
		};
	});

	if (pickItems.length === 0) {
		vscode.window.showErrorMessage('Failed to find "pubspec.yaml" file');
		return;
	}

	if (pickItems.length > 1) {
		// prepare to eventually serve multiple apps
		// pickItems.splice(0, 0, { label: 'All', description: 'Serve all project listed below' });
		const result = await vscode.window.showQuickPick(pickItems);
		if (!result) {
			pickItems.splice(0, pickItems.length);
		}
		// prepare to eventually serve multiple apps
		// else if (result == pickItems[0]) {
		// 	pickItems.splice(0, 1);
		// }
		else {
			pickItems.splice(0, pickItems.length, result);
		}
	}

	for (let item of pickItems) {
		runCommandInIntegratedTerminal([ "serve", "--auto", "refresh" ], item.folder.uri.fsPath);
	}
}

function runCommandInIntegratedTerminal(args: string[], cwd: string | undefined): void {
	const cmd_args = Array.from(args);

	if (!terminal) {
		terminal = vscode.window.createTerminal('webdev');
	}
	terminal.show();
	if (cwd) {
		// Replace single backslash with double backslash.
		const textCwd = cwd.replace(/\\/g, '\\\\');
		terminal.sendText(['cd', `"${textCwd}"`].join(' '));
	}
	cmd_args.splice(0, 0, getWebdevBin());
	terminal.sendText(cmd_args.join(' '));
}

function getWebdevBin(): string {
	return vscode.workspace.getConfiguration('webdev')['bin'] || 'webdev';
}