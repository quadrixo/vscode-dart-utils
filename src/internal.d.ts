import * as vscode from "vscode";

export interface ProjectQuickPickItem extends vscode.QuickPickItem {
    folder: vscode.WorkspaceFolder;
}