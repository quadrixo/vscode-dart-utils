# vscode-dart-utils
 > Dart development utilities

## installing

```shell
ext install qp.vscode-dart-utils
```

## Commands

 * __webdev: serve__: Run `webdev serve --auto refresh`

## Licensing

The code in this project is licensed under CeCILL license. See LICENSE.txt file for more information.