## 0.1.1 (2019-11-12)
### Fixed
 * Dependency @types/vscode

### Changed
 * Refactor the logo

## 0.1.0 (2019-11-12)
### Added
 * Add 'webdev serve' command
